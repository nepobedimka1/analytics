from airflow import DAG
from airflow.operators import PythonOperator
from datetime import datetime

default_args = {
    'owner': 'nepobedimka',
    'depends_on_past': False,
    'start_date': datetime(2021, 6, 24),
    'retries': 0
}
dag = DAG('test',
          default_args=default_args,
          catchup=False,
          schedule_interval='00 * * * *'
          )


def send_report():
    import pandas as pd
    import numpy as np
    import vk_api
    import random
    import telebot
    import requests
    ads = pd.read_csv('ads_data_121288.csv', parse_dates=[0])
    print('данные считаны')

    ads_view = ads[ads['event'] == 'view'].groupby(['date', 'ad_id']).count().reset_index()[['date', 'ad_id', 'event']]
    ads_view.columns = ['date', 'ad_id', 'view']

    ads_click = ads[ads['event'] == 'click'].groupby(['date', 'ad_id']).count().reset_index()[
        ['date', 'ad_id', 'event']]
    ads_click.columns = ['date', 'ad_id', 'clicks']

    ads_ctr = pd.merge(ads_click, ads_view, on=['date', 'ad_id'])
    ads_ctr

    ads_ctr['CTR'] = ads_ctr['clicks'] / ads_ctr['view']

    ads['ad_action_cost'] = ads['ad_cost'] / 1000

    ads_ctr['money'] = ads_ctr['view'] * ads.ad_action_cost.unique()[0]

    money_0204 = float(ads_ctr[ads_ctr['date'] == '2019-04-02']['money'])
    view_0204 = float(ads_ctr[ads_ctr['date'] == '2019-04-02']['view'])
    clicks_0204 = float(ads_ctr[ads_ctr['date'] == '2019-04-02']['clicks'])
    CTR_0204 = float(ads_ctr[ads_ctr['date'] == '2019-04-02']['CTR'])

    money_0104 = float(ads_ctr[ads_ctr['date'] == '2019-04-01']['money'])
    view_0104 = float(ads_ctr[ads_ctr['date'] == '2019-04-01']['view'])
    clicks_0104 = float(ads_ctr[ads_ctr['date'] == '2019-04-01']['clicks'])
    CTR_0104 = float(ads_ctr[ads_ctr['date'] == '2019-04-01']['CTR'])

    diff_money = round((money_0204 - money_0104) / money_0104 * 100)
    diff_view = round((view_0204 - view_0104) / view_0104 * 100)
    diff_clicks = round((clicks_0204 - clicks_0104) / clicks_0104 * 100)
    diff_CTR = round((CTR_0204 - CTR_0104) / CTR_0204 * 100)
    print('Метрики посчитаны')
    message = f'''отчет по обьявлению 121288 за 2 апреля \n
    Траты: {money_0204} ({diff_money}%)
    Показы:{view_0204} ({diff_view}%)
    Клики:{clicks_0204} ({diff_clicks}%)
    СTR:{CTR_0204} ({diff_CTR}%)



    '''

    bot_token = '1401270918:AAF3Xp8vyx8PEDrE3K62PMPjnEnqHJKqwjE'
    chatID = '774448062'
    url = f'https://api.telegram.org/bot{bot_token}/sendMessage?chat_id={chatID}&text={message}'
    response = requests.get(url)
    print('отчет отправлен')

    t1 = PythonOperator(task_id='ads_report',
                        python_callable=send_report,
                        dag=dag)

